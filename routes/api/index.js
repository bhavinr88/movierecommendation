// Include dependent node modules and helpres
var express = require('express');
var router = express.Router();
const { check, validationResult, body } = require('express-validator');
const axios = require('axios');
const moment = require("moment");
let auth_helper = require('../../helpers/auth');
let auth = auth_helper.functions();

/**
 * Get get movie recommendation based on user inputs
 * Search by genre and time
 * Default sorting by highest rating
 * @genre 
 * @time = should be 24hr format valid time
 */
router.post('/find-movie', [
    check('genre').not().isEmpty().withMessage("Genre is required."),
    check('time').not().isEmpty().withMessage("Time is required.").matches(/^$|^(([01][0-9])|(2[0-3])):[0-5][0-9]$/).withMessage("Invalid Time.")
  ], auth.verifyToken, (req, res, next) => {
    let result = [];
    //Validate api params
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        let body_errors = []
        errors.array().map(elm => {
            body_errors.push({
                "param" : elm.param,
                "error": elm.msg
            })
        });
      return res.status(422).json({ response_code: 0, errors: body_errors })
    }
    let post = req.body;
    // Fetch movie list
    axios.get(process.env.MOVIE_API)
    .then(response => {
        // console.log(response.data)
        let movie_arr = filterGenre(post.genre, response.data);
        let time = post.time.split(':');
        let hour = time[0];
        let minute = time[1];
        var cdate = new Date;
        cdate = cdate.setHours(hour, minute, '00')
        let cdate_time = moment(cdate).add(30, 'minutes').unix();
        movie_arr = filterTime(cdate_time, movie_arr);
        if(movie_arr.length >= 1){            
            // sort by highest rating
            movie_arr.sort((a,b) => {
                if ( a.rating < b.rating ){
                    return 1;
                }
                if ( a.rating > b.rating ){
                    return -1;
                }
                return 0;
            });
            result ={ 
                response_code : 1,
                response_msg:"Success",
                data : movie_arr
            };
        } else {
            result ={ 
                response_code: 0,
                response_msg:"No movie recommendation."
            };
        }        
        return res.status(200).json(result);
    })
    .catch(error => {
        return res.status(500).json({error : 'Internal server error'});
    });
    
});
module.exports = router;

/**
 * Helper function to find matched genre from array 
 * @keyword = genre name 
 * @arr = An array of movies
 */
function filterGenre(keyword, arr){
    let result = [];
    for (var i=0; i < arr.length; i++) {
        for (var j=0; j < arr[i].genres.length; j++) {
            if (arr[i].genres[j].toLowerCase() === keyword.toLowerCase()) {
                result.push(arr[i]);
            }
        }
    }
    return result;
}

/**
 * Helper function to find matched timings from array 
 * @search_time = 24 our format time
 * @arr = An array of movies
 */
function filterTime(search_time, arr){
    let result = [];
    for (var i=0; i < arr.length; i++) {
        for (var j=0; j < arr[i].showings.length; j++) {
            let time = arr[i].showings[j].split(':');
            let hour = time[0];
            let minute = time[1];
            var cdate = new Date;
            cdate = cdate.setHours(hour, minute, '00')
            let cdate_time = moment(cdate).unix();
            if (cdate_time >= search_time) {
                arr[i].showings = moment(cdate).format('LT');
                result.push(arr[i]);
            }
        }
    }
    return result;
}