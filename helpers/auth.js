exports.functions = function () {
    return {
        verifyToken: function (req, res, next) {
            var token = req.headers['x-access-token'];
            if (typeof token !== 'undefined' && token == process.env.AUTH_TOKEN) {
                next();
            } else {
                return res.status(401).json({
                    status: -1,
                    message: 'Invalid authentication token.'
                });
            }
        }
    };
}