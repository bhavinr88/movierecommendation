## movierecommendation

Node.js test application to get movie recommendation

## Requirements

* Node 8+

## Common setup

Clone the repo and install the dependencies.

```bash
git clone https://bhavinr88@bitbucket.org/bhavinr88/movierecommendation.git 
cd movierecommendation
```

```bash
npm install
```

## Steps to run app

To start the express server, run the following

```bash
node app.js
```

Open [http://localhost:3000/api-docs/](http://localhost:3000/api-docs/) and take a look around.


## Steps to run test

```bash
node app.js
```
