const request = require('supertest')
const app = require('../app')
describe('Movie Recommendation Endpoints', () => {
  let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9';
  let invalid_token = 'eyIsInR5cCI6IkpXVCJ9';

  //Authorization
  it('should return authorization error with response code 401', async () => {
    const res = await request(app)
      .post('/api/find-movie')
      .send({
        genre: 'Comedy',
        time: '19:30',
      })
      .set('x-access-token', invalid_token)
    expect(res.statusCode).toEqual(401)
  })

  //Validation
  it('should return validation errors', async () => {
    const res = await request(app)
      .post('/api/find-movie')
      .send({
        genre:'',
        time: '4:00',
      })
      .set('x-access-token', token)
    expect(res.statusCode).toEqual(422)
    expect(res.body).toHaveProperty('errors')
  })

  //No data
  it('should not return any data but success response', async () => {
    const res = await request(app)
      .post('/api/find-movie')
      .send({
        genre: 'Fiction',
        time: '19:30',
      })
      .set('x-access-token', token)
    expect(res.statusCode).toEqual(200)
    expect(res.body.response_code).toEqual(0)
    expect(res.body.response_msg).toEqual('No movie recommendation.')
  })

  //Success
  it('should return data', async () => {
    const res = await request(app)
      .post('/api/find-movie')
      .send({
        genre: 'Animation',
        time: '19:00',
      })
      .set('x-access-token', token)
    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('data')
  })
})